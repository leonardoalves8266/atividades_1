//comentar 
/*
Faça um programa que receba quatro números inteiros,
calcule e mostre a soma desses números.
*/

//primeiro bloco
//inicio
namespace exercicio_1
{
    //entrada dos dados
    //var --- let --- const
    let numero1, numero2, numero3, numero4: number;
    numero1 = 5;
    numero2 = 10;
    numero3 = 15;
    numero4 = 20;
    
    let resultado: number;
    
    //processar os dados 
    resultado = numero1 + numero2 + numero3 + numero4;

    //saida 
    console.log ("o resultado da soma e:" + resultado);
    //ou pode-se escrever
    console.log(`o resultado da soma e ${resultado}`);
}




