/*10) Área de um círculo

Faça um programa que calcule e mostre a área de um
círculo. Sabe-se que: Área = π.R2
*/
namespace exercicio_10
{
    let pi, raio: number
    pi = 3,14;
    raio = 10;

    let area: number;
    area = pi * Math.pow(raio, 2);

    console.log(`a area e: ${area}`)
}

