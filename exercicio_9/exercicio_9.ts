/*9) Área do triângulo

Faça um programa que calcule e mostre a área de um
triângulo. Sabe-se que: Área = (base * altura)/2.
*/
namespace exercicio_9
{
    let altura, base: number;
    altura = 10;
    base = 8;
    
    let area: number;
    area =(base * altura) / 2;

    console.log(`a area e: ${area}`);
}

