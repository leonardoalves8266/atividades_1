/*3) Média Ponderada

Faça um programa que receba três notas, calcule e mostre a
média ponderada entre elas.
*/
    //primeir bloco
    //inicio
    namespace exercicio_3
    //entrada
    {
    let nota1, nota2, nota3, peso1, peso2, peso3: number;
    nota1= 10
    nota2= 5
    nota3= 7
    peso1= 1 
    peso2= 2
    peso3= 3
    let media: number
    //processo
    media = (nota1 * peso1 + nota2 * peso2 + nota3 * peso3) / (peso1 + peso2 + peso3)
    //saida
    console.log (`media do aluno e: ${media}`)
    }