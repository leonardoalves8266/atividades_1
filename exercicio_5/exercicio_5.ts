/*5) Cálculo de salário

Faça um programa que receba o salário de um funcionário e
o percentual de aumento, calcule e mostre o valor do
aumento e o novo salário.
*/

    //primeiro bloco
    //inicio
    namespace exercicio_05
    {
    let salario, aumento

    salario = 15000
    aumento = 13.5/100

    let novo_salario

    //processo

    novo_salario = salario * aumento + salario

    //saida

    console.log (`o seu salario com o aumento sera: ${novo_salario}`)
    
    }
