/*7) Cálculo de salário

Faça um programa que receba o salário-base de um

funcionário, calcule e mostre o seu salário a receber, sabendo-
se que esse funcionário tem gratificação de R$50,00 e paga

imposto de 10% sobre o salário-base.
*/
    namespace exercicio_07
    {
    let salario_base: number;
    let salario_bonificacao: number;
    let salario_imposto: number;
    let salario_final: number;
    
    salario_base = 5000
    salario_bonificacao = 70.00

    salario_imposto = salario_base * 10/100
    salario_final = salario_base + salario_bonificacao;

    console.log(`o seu salario e: ${salario_final}`);
    
    }
